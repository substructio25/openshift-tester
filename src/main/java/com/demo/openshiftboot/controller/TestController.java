package com.demo.openshiftboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class TestController {
    @RequestMapping("/sayHello")
    public String sayHello(){
        return "Hello User";
    }

    @RequestMapping("/sayMine")
    public String sayMine(){
        return "Say Mine";
    }
}
