package com.demo.openshiftboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenshiftBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenshiftBootApplication.class, args);
	}

}
